( function( $ ) {


    //the id works like a grid, 'sq' stands for 'sidequote'
    //11 = top left quote
    //12 = top right quote
    //31 = bottom left quote
    // not elegant? I agree
    wp.customize( 'services_quote_left', function( value ) {
        value.bind( function( newval ) {
            $('#sq11').text( newval );
        } );
    } );
    wp.customize( 'services_quote_right', function( value ) {
        value.bind( function( newval ) {
            $('#sq12').text( newval );
        } );
    } );
    wp.customize( 'getquote_quote_left', function( value ) {
        value.bind( function( newval ) {
            $('#sq21').text( newval );
        } );
    } );
    wp.customize( 'getquote_quote_right', function( value ) {
        value.bind( function( newval ) {
            $('#sq22').text( newval );
        } );
    } );
    wp.customize( 'career_quote_left', function( value ) {
        value.bind( function( newval ) {
            $('#sq31').text( newval );
        } );
    } );
    wp.customize( 'career_quote_right', function( value ) {
        value.bind( function( newval ) {
            $('#sq32').text( newval );
        } );
    } );

} )( jQuery );
