var salt = {};

(function( doc, salt ){
    salt.scrollTo = function ( name ) {
            $( 'html, body' ).animate({
                scrollTop: $( name ).offset().top
            }, 1000 );
    };

    salt.doLoading = function () {
        if ( doc.readyState == "complete" ) {
            $("#curtain").fadeTo( 1000, 0 );
            setTimeout(function(){ $("#curtain").remove() }, 600 );
        } else {
            setTimeout( salt.doLoading, 100 );
        }
    }


})(document, salt);
