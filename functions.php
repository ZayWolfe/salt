<?php

//inlude large sections
require_once("inc/shortcodes.php");
require_once("inc/customizer.php");
require_once("inc/forms.php");

//register the main menu
function salt_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}

add_action( 'init', 'salt_menu' );

// internal template functions
function salt_get_page ( $page_title ) {
    $page = get_page_by_title($page_title);
    $content = $page->post_content;
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]>', $content);
    return $content;
}

?>
