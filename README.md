# Salt

The goal of salt is to create a website that has a focus on utilizing photography
to humanize a company within a one page design.

One page design:

The one page design idea has been very popular lately in its simplicity. Everything
is rendered in one large page but sectioned in such a way that each important part
feels unique.

Utilization:

The website will use Wordpress as its base to allow the site to be dynamic and
so a whole new backend won't have to be created. It'll allow easy and quick changes
with a web interface without the extended development time to reinvent the wheel.
