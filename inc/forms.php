<?php

class SaltForm {
    //mailing vars
    public static $to_email         = "gilliland.isaiah@gmail.com";
    //response messages
    public static $response         = "";
    public static $not_human        = "Human verification incorrect.";
    public static $missing_content  = "Please supply all information.";
    public static $email_invalid    = "Email Address Invalid.";
    public static $message_unsent   = "Message was not sent. Try Again.";
    public static $message_sent     = "Thanks! Your message has been sent.";
    public static $wrong_file       = "Resume is wrong format. pdf/doc/docx only";
    //user posted variables
    public static $name             = "";
    public static $email            = "";
    public static $company          = "";
    public static $message_quote    = "";
    public static $message_career   = "";
    public static $file             = "";
    public static $filename         = "";
    public static $human            = "";
    public static $form_type        = "";

    public static function get_post () {
        //get the basic post values
        self::$name             = $_POST['message_name'];
        self::$email            = $_POST['message_email'];
        self::$company          = $_POST['message_company'];
        self::$message_quote    = $_POST['message_quote_text'];
        self::$message_career   = $_POST['message_career_text'];
        self::$human            = $_POST['message_human'];
        self::$form_type        = $_POST['message_type'];

        //check for file and get important information
        if( self::$form_type == "career" ){
            if ( empty( $_FILES["attachement"] ) ) {
                self::$file = NULL;
            } else {

                self::$file     = $_FILES["attachement"]["tmp_name"];
                self::$filename = $_FILES["attachement"]["name"];

            }
        }

    }

    public static function validate () {

        //fix differences between forms
        $human_compare = 0;
        $message = "";

        if ( self::$form_type == "quote" ) {
            $human_compare = 2;
            $message = SaltForm::$message_quote;
        } else if ( self::$form_type == "career" ) {
            $human_compare = 4;
            $message = SaltForm::$message_career;
        } else {
            //set the default text in the "get a quote" textarea
            SaltForm::$message_quote = "Interested Services:\n\n\nDescription:\n\n\n*Phone Number* and Additional Contact Info:\n";
            return false;
        }

        //validate human input
        if ( SaltForm::$human == 0) {
            salt_set_form_response( "error", SaltForm::$missing_content );
            return false;
        } else if( SaltForm::$human != $human_compare ){
            salt_set_form_response("error", SaltForm::$not_human); //not human!
            return false;
        }

        //validate email
        if( !filter_var( SaltForm::$email, FILTER_VALIDATE_EMAIL ) ) {
            salt_set_form_response("error", SaltForm::$email_invalid); //badly typed email
            return false;
        }

        //validate name and message
        if( empty( SaltForm::$name ) || empty( $message ) ) {
            salt_set_form_response( "error", SaltForm::$missing_content );
            return false;
        }

        //validate file extension
        if( !empty(self::$file ) ){
            $allowed = array("pdf", "doc", "docx");//allowed mime types
            $ext =  pathinfo( self::$filename, PATHINFO_EXTENSION );

            if( !in_array( $ext, $allowed ) ) {
                salt_set_form_response("error", self::$wrong_file); //wrong file extension
                return false;
            }

        }

        //all set
        return true;
    }

    public static function send_email () {
        require_once ABSPATH . WPINC . '/class-phpmailer.php';

        $mailer = new PHPMailer();
        $was_sent = false;

        //preparing email
        $mailer->FromName = SaltForm::$name;
        $mailer->From = SaltForm::$email;
        $mailer->AddReplyTo( SaltForm::$email );
        $mailer->AddAddress( SaltForm::$to_email );

        if( SaltForm::$form_type == "quote") {
            if( !empty( SaltForm::$company ) )
                $mailer->Subject = "Quote Request from - " . SaltForm::$name . " at " . SaltForm::$company ;
            else
                $mailer->Subject = "Quote Request from - " . SaltForm::$name;

            $mailer->Body = trim(strip_tags( SaltForm::$message_quote ));

            $was_sent = $mailer->Send();
        } else { //process an application/career email

            $mailer->Subject = "Quote Request from - " . SaltForm::$name;
            $mailer->Body = trim(strip_tags( SaltForm::$message_career ));

            if ( !empty(SaltForm::$file) )
                $mailer->AddAttachment( SaltForm::$file, SaltForm::$filename );

            $was_sent = $mailer->Send();
        }

        if($was_sent) {
            salt_set_form_response("success", SaltForm::$message_sent); //message sent!
        } else {
            salt_set_form_response("error", SaltForm::$message_unsent); //message wasn't sent
        }
    }
}

function salt_set_form_response ($type, $message) {
    if ( $type == "success" )
        SaltForm::$response = "<div class='success'>{$message}</div>";
    else
        SaltForm::$response = "<div class='error'>{$message}</div>";
}

function salt_get_quote_form() {

    $form_action = get_permalink();
    $form_name = SaltForm::$name;
    $form_company = SaltForm::$company;
    $form_email = SaltForm::$email;
    $form_textarea = SaltForm::$message_quote;
    $form_response = "";

    //get the response from the right form
    if ( SaltForm::$form_type == "quote" ) {
        $form_response = SaltForm::$response . "<script>salt.scrollTo('#v3');</script>";
    }


$markup = <<<EOT

                            {$form_response}
                            <form action="{$form_action}" method="post">
                                <p><label for="name">Name: <span>*</span>
                                        <br><input type="text" name="message_name" placeholder="Jane Doe" value="{$form_name}">
                                </label></p>
                                <p><label for="name">Company Name:
                                        <br><input type="text" name="message_company" placeholder="Example, LLC." value="{$form_company}">
                                </label></p>
                                <p><label for="message_email">Email: <span>*</span>
                                    <br><input type="text" name="message_email" placeholder="jane.doe@example.com" value="{$form_email}">
                                </label></p>
                                <p><label for="message_text">Message: <span>*</span>
                                    <br><textarea type="text" rows="10" name="message_quote_text">{$form_textarea}</textarea>
                                </label></p>
                                <p><label for="message_human">Human Verification: <span>*</span>
                                    <br><input type="text" style="width: 60px;" name="message_human"> + 3 = 5
                                </label></p>

                                <input type="hidden" name="message_type" value="quote">

                                <p><input type="submit"></p>
                            </form>

EOT;

    return $markup;


}

function salt_get_career_form() {

    $form_action = get_permalink();
    $form_name = SaltForm::$name;
    $form_company = SaltForm::$company;
    $form_email = SaltForm::$email;
    $form_textarea = SaltForm::$message_career;
    $form_response = "";

    //get the response from the right form
    if ( SaltForm::$form_type == "career" ) {
        $form_response = SaltForm::$response . "<script>salt.scrollTo('#v4');</script>";
    }


$markup = <<<EOT

                            {$form_response}
                            <form action="{$form_action}" method="post" enctype="multipart/form-data" >
                                <p><label for="name">Name: <span>*</span>
                                        <br><input type="text" name="message_name" placeholder="Jane Doe" value="{$form_name}">
                                </label></p>
                                <p><label for="message_email">Email: <span>*</span>
                                    <br><input type="text" name="message_email" placeholder="jane.doe@example.com" value="{$form_email}">
                                </label></p>
                                <p><label for="message_text">Cover Letter: <span>*</span>
                                    <br><textarea type="text" rows="10" name="message_career_text">{$form_textarea}</textarea>
                                </label></p>
                                <p><label for="name">Resume: pdf/doc/docx
                                        <br><input type="file" name="attachement" >
                                </label></p>
                                <p><label for="message_human">Human Verification: <span>*</span>
                                    <br><input type="text" style="width: 60px;" name="message_human"> + 4 = 8
                                </label></p>

                                <input type="hidden" name="message_type" value="career">

                                <p><input type="submit"></p>
                            </form>

EOT;

    return $markup;


}

?>
