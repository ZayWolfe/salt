<?php

//register the customizer options
function salt_theme_options( $wp_customize ) {
   //the sidequote section
   $wp_customize->add_section("salt_sidequote_settings", array(
       'title'      => __("Side Quotes", "Salt"),
       'priority'   => 1,
       'capability'  => 'edit_theme_options',
       'description' => __('Change the quotes on the left and right sides here.', 'Salt'),
   ));

   //the sidequotes settings
   $wp_customize->add_setting("services_quote_left", array(
       'default'    => "Lorem ipsum dolor sit amet, congue diceret cu qui, est et atqui maiestatis, at veniam prompta eripuit vim",
       'transport'  => 'postMessage'
   ));
   $wp_customize->add_setting("services_quote_right", array(
       'default'    => "Lorem ipsum dolor sit amet, congue diceret cu qui, est et atqui maiestatis, at veniam prompta eripuit vim",
       'transport'  => 'postMessage'
   ));
   $wp_customize->add_setting("getquote_quote_left", array(
       'default'    => "Lorem ipsum dolor sit amet, congue diceret cu qui, est et atqui maiestatis, at veniam prompta eripuit vim",
       'transport'  => 'postMessage'
   ));
   $wp_customize->add_setting("getquote_quote_right", array(
       'default'    => "Lorem ipsum dolor sit amet, congue diceret cu qui, est et atqui maiestatis, at veniam prompta eripuit vim",
       'transport'  => 'postMessage'
   ));
   $wp_customize->add_setting("career_quote_left", array(
       'default'    => "Lorem ipsum dolor sit amet, congue diceret cu qui, est et atqui maiestatis, at veniam prompta eripuit vim",
       'transport'  => 'postMessage'
   ));
   $wp_customize->add_setting("career_quote_right", array(
       'default'    => "Lorem ipsum dolor sit amet, congue diceret cu qui, est et atqui maiestatis, at veniam prompta eripuit vim",
       'transport'  => 'postMessage'
   ));

   //add the controls to the customizer menu
   $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        "services_quote_left_ctr",
        array(
           'label'      => __( 'Edit quote for Services, left', 'Salt' ),
           'section'    => 'salt_sidequote_settings',
           'settings'   => 'services_quote_left',
           'type'       => 'textarea',
           'priority'   => 10,
       )
   ));
   $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        "services_quote_right_ctr",
        array(
           'label'      => __( 'Edit quote for Services, right', 'Salt' ),
           'section'    => 'salt_sidequote_settings',
           'settings'   => 'services_quote_right',
           'type'       => 'textarea',
           'priority'   => 20,
       )
   ));
   $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        "getquote_quote_left_ctr",
        array(
           'label'      => __( 'Edit quote for Get A Quote, left', 'Salt' ),
           'section'    => 'salt_sidequote_settings',
           'settings'   => 'getquote_quote_left',
           'type'       => 'textarea',
           'priority'   => 30,
       )
   ));
   $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        "getquote_quote_right_ctr",
        array(
           'label'      => __( 'Edit quote for Get A Quote, right', 'Salt' ),
           'section'    => 'salt_sidequote_settings',
           'settings'   => 'getquote_quote_right',
           'type'       => 'textarea',
           'priority'   => 40,
       )
   ));
   $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        "career_quote_left_ctr",
        array(
           'label'      => __( 'Edit quote for Careers, left', 'Salt' ),
           'section'    => 'salt_sidequote_settings',
           'settings'   => 'career_quote_left',
           'type'       => 'textarea',
           'priority'   => 50,
       )
   ));
   $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        "career_quote_right_ctr",
        array(
           'label'      => __( 'Edit quote for Careers, right', 'Salt' ),
           'section'    => 'salt_sidequote_settings',
           'settings'   => 'career_quote_right',
           'type'       => 'textarea',
           'priority'   => 60,
       )
   ));
}

function salt_theme_preview () {
    wp_enqueue_script(
        'my_theme_customizer',
        get_template_directory_uri() . '/js/theme-preview.js',
        array(  'jquery', 'customize-preview' ),
        '',
        true
    );

}

//add hooks
add_action( 'customize_register', 'salt_theme_options' );
add_action( 'customize_preview_init' , 'salt_theme_preview' );

?>
