<?php


function salt_quote_button () {

    $markup = <<<EOT

<div class="quote_button"><a onclick="salt.scrollTo('#v3')" >Filler</a></div>

EOT;

    return $markup;
}

add_shortcode( 'forward_to_quote', 'salt_quote_button' );

///////// we can fix it
function salt_we_fix_it () {
    $path = get_stylesheet_directory_uri();

    $markup = <<<EOT

<img src="{$path}/images/wefixit.png" width="100%" />

EOT;

    return $markup;
}

add_shortcode( 'wefixit', 'salt_we_fix_it' );


///////// who we are
function salt_we_are_hhe () {
    $path = get_stylesheet_directory_uri();

    $markup = <<<EOT

<img src="{$path}/images/weare.png" width="100%" />

EOT;

    return $markup;
}

add_shortcode( 'wearehhe', 'salt_we_are_hhe' );

///////// supported brands
function salt_supported_brands () {
    $path = get_stylesheet_directory_uri();

    $markup = <<<EOT

<img src="{$path}/images/brands.png" width="100%" />

EOT;

    return $markup;
}

add_shortcode( 'brandswesupport', 'salt_supported_brands' );


///////// we have parts
function salt_we_have_parts () {
    $path = get_stylesheet_directory_uri();

    $markup = <<<EOT

<img src="{$path}/images/parts.png" width="100%" />

EOT;

    return $markup;
}

add_shortcode( 'wehaveparts', 'salt_we_have_parts' );

?>
