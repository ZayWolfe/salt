<!DOCTYPE HTML>
<HTML>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" ./>
        <meta http-equiv="content-type" content="text/html" />
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta name="description" content="Hidden Hill Equipment Group is a life line to anyone with after market heavy duty
         equipment. Why pay crazy prices to upgrade when what you have will works just as good? Get a quote today and let
        us solve all your after market equpment needs. Parts, repairs, or custom work are welcome." />
        <meta property="og:title" content="Hidden Hills Equipment Group | solve your after market needs" />
        <meta property="og:type" content="website" />
        <meta property="og:image" content="put image here" />
        <meta property="og:url" content="http://www.hhequip.com" />
        <meta property="og:description" content="So, you have heavy duty after market equipment. How can we make your life
        easier? Repairs, parts, cutom work, all your needs are welcome. Get a quote now!" />
        <meta property="twitter:url" content="http://www.hhequip.com" />
        <meta property="twitter:title" content="Hidden Hills Equipment Group | solve your after market needs" />
        <meta property="twitter:description" content="So, you have heavy duty after market equipment. How can we make your life
        easier? Repairs, parts, cutom work, all your needs are welcome. Get a quote now!" />
        <meta property="twitter:image" content="put image here" />

        <title><?php wp_title(); ?></title>
        <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="all" />
        <!--[if lte IE 8]>
            <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style-ie.css" type="text/css" media="all" />
        <![endif]-->
        <script charset="utf-8" src="<?php bloginfo('template_url'); ?>/js/jquery-1.11.2.min.js" ></script>
        <script charset="utf-8" src="<?php bloginfo('template_url'); ?>/js/respond.js" ></script>
        <script charset="utf-8" src="<?php bloginfo('template_url'); ?>/js/salt.js" ></script>
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php wp_head(); ?>

    </head>
