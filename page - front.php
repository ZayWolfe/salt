<?php
/*
Template Name: FrontPage
*/
?>

<?php
// form handling
SaltForm::get_post();
if( SaltForm::validate() ) {
    SaltForm::send_email();
}
?>

<?php get_header(); ?>

<body>

    <div id="curtain">
        <img src="<?php bloginfo('template_url'); ?>/images/loading.gif"></img>
    </div>
    <div id="tinting"></div>

    <script>
        salt.doLoading();
    </script>

    <div id="page">
        <div id="header">
            <div id="motto"><span class="toplogo"></span> <span class="sitetitle">Hidden Hills Equipment Group, LLC.</span></div>
            <div id="nav">
                <ul class="mainmenu">
                    <li class="menuitem active" onclick="salt.scrollTo('#v1')" ><span>Services</span></li>
                    <li class="menuitem" onclick="salt.scrollTo('#v2')" ><span>Who We Are</span></li>
                    <li class="menuitem" onclick="salt.scrollTo('#v3')" ><span>Get A Quote</span></li>
                    <li class="menuitem" onclick="salt.scrollTo('#v4')" ><span>Careers</span></li>
                    <li class="menuitem" onclick="salt.scrollTo('#v5')" ><span>Contact</span></li>
                </ul>
            </div>
        </div>



        <div id="v1" class="section child1">
            <div class="filler">

                <div class="sidequote">
                    <div class="sidebox">
                        <div class="sidedivider">--------</div>
                        <div id="sq11" class="sidetext"><?php
                            echo get_theme_mod('services_quote_left', 'Please type quote in theme settings.');
                        ?></div>
                        <div class="sidedivider">--------</div>
                    </div>
                </div>

                <div class="page_holder">
                    <div class="sidebar">
                        <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
                    </div>
                    <div class="paper">
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <div class="title"><h2>Services</h2></div>
                        <div class="article">
                          <div class="picture1" ></div>
                          <?php the_content(); ?>
                          <?php endwhile; ?>
                          <?php endif; ?>
                        </div>
                    </div>
                </div>

                <div class="sidequote">
                    <div class="sidebox">
                        <div class="sidedivider">--------</div>
                        <div id="sq12" class="sidetext"><?php
                            echo get_theme_mod('services_quote_right', 'Please type quote in theme settings.');
                        ?></div>
                        <div class="sidedivider">--------</div>
                    </div>
                </div>

            </div>
        </div>

        <div id="v2" class="section child2">
            <div class="filler">

                <div class="sidequote">
                    <div class="sidebox">
                        <div class="sidedivider"></div>
                        <div class="sidetext"></div>
                        <div class="sidedivider"></div>
                    </div>
                </div>

                <div class="page_holder">
                    <div class="paper">
                        <div class="title"><h2>Who We Are</h2></div>
                        <div class="article bare">
                            <?php echo salt_get_page( "about" ); ?>
                        </div>
                        <div class="horigallery">

                        </div>
                    </div>
                </div>

                <div class="sidequote">
                    <div class="sidebox">
                        <div class="sidedivider"></div>
                        <div class="sidetext"></div>
                        <div class="sidedivider"></div>
                    </div>
                </div>

            </div>
        </div>

        <div id="v3" class="section child3">
            <div class="filler">

                <div class="sidequote">
                    <div class="sidebox">
                        <div class="sidedivider">--------</div>
                        <div id="sq21" class="sidetext"><?php
                            echo get_theme_mod('getquote_quote_left', 'Please type quote in theme settings.');
                        ?></div>
                        <div class="sidedivider">--------</div>
                    </div>
                </div>

                <div class="page_holder">
                    <div class="paper">
                        <div class="title"><h2>Get a Quote</h2></div>
                        <div class="article bare">
                            <?php echo salt_get_page( "quote form" ); ?>
                            <?php echo salt_get_quote_form(); ?>
                        </div>
                    </div>
                </div>

                <div class="sidequote">
                    <div class="sidebox">
                        <div class="sidedivider">--------</div>
                        <div id="sq22" class="sidetext"><?php
                            echo get_theme_mod('getquote_quote_right', 'Please type quote in theme settings.');
                        ?></div>
                        <div class="sidedivider">--------</div>
                    </div>
                </div>

            </div>
        </div>

        <div id="v4" class="section child4">
            <div class="filler">

                <div class="sidequote">
                    <div class="sidebox">
                        <div class="sidedivider">--------</div>
                        <div id="sq31" class="sidetext"><?php
                            echo get_theme_mod('career_quote_left', 'Please type quote in theme settings.');
                        ?></div>
                        <div class="sidedivider">--------</div>
                    </div>
                </div>

                <div class="page_holder">
                  <div class="paper">
                      <div class="title"><h2>Careers</h2></div>
                      <div class="article bare">
                          <?php echo salt_get_page( "career form" ); ?>
                          <?php echo salt_get_career_form(); ?>
                      </div>
                  </div>
                </div>

                <div class="sidequote">
                    <div class="sidebox">
                        <div class="sidedivider">--------</div>
                        <div id="sq32" class="sidetext"><?php
                            echo get_theme_mod('career_quote_right', 'Please type quote in theme settings.');
                        ?></div>
                        <div class="sidedivider">--------</div>
                    </div>
                </div>

            </div>
        </div>

        <div id="v5" class="section child5">
            <div class="filler">
                <div class="contact-holder">
                    <?php echo salt_get_page( "contact" ); ?>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>
